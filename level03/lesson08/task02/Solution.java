package com.javarush.test.level03.lesson08.task02;

/* Зарплата через 5 лет
Ввести с клавиатуры Имя и два числа, вывести надпись:
name1 получает «число1» через «число2» лет.
Пример: Коля получает 3000 через 5 лет.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        String a = reader.readLine();
        String b = reader.readLine();
        int e = Integer.parseInt(b);
        String c = reader.readLine();
        int f = Integer.parseInt(c);


        System.out.println(a+ " получает " + e +" через " + f + " лет.");

    }
}